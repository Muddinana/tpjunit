package polynomes;
import org.junit.jupiter.api.Test;
import polynomes.Monome;
import polynomes.Polynome;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
public class TestPolynome {

    @Test
    public void testAjoutMonome(){
        Monome m0=new Monome(5,0);
        Monome m1=new Monome(3,1);
        Monome m2=new Monome(3,2);
        Monome m3=new Monome(5,7);

        Polynome p=new Polynome();
        p.ajoutMonome(m1);
        assertEquals("+3x",p.toString());
        p.ajoutMonome(m2);
        assertEquals("+3x^2+3x",p.toString());
        p.ajoutMonome(m0);
        assertEquals("+3x^2+3x+5",p.toString());
        p.ajoutMonome(m3);
        assertEquals("+5x^7+3x^2+3x+5",p.toString());

    }
    @Test
    public void testProduit(){
        Monome m0=new Monome(5,0);
        Monome m1=new Monome(3,1);
        Monome m2=new Monome(3,2);
        Monome m3=new Monome(2,2);

        Polynome p=new Polynome();
        p.ajoutMonome(m0);
        p.ajoutMonome(m1);
        p.ajoutMonome(m2);

        Polynome p1=p.produit(m3);

        assertEquals("+6x^4+6x^3+10x^2",p1.toString());
    }


}
