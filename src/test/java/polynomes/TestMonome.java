package polynomes;
import org.junit.jupiter.api.Test;
import polynomes.DegreNegatifException;
import polynomes.Monome;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestMonome {

    @Test
    public void testToString4_0(){
        assertEquals("+4",new Monome(4,0).toString());
    }

    @Test
    public void testToString4_1(){
        assertEquals("+4x",new Monome(4,1).toString());
    }

    @Test
    public void testDegNeg(){
        assertThrows(DegreNegatifException.class,()-> {new Monome(4,-1);});
    }

}
